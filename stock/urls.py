import requests
from django.contrib import admin
from django.conf.urls import url, include, handler404
from django.urls import include, path
from rest_framework.views import APIView

class MyView(APIView):
    def get(self, request, num1, num2):
        return requests.Response({str(num1) + str(num2): num1+num2})

urlpatterns = [
    path('', include('recommendation.urls')),
    path('admin/', admin.site.urls),
    path('test/', MyView.as_view())
]