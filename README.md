# Stock Trade

Provides stock market trading insights and recommendations, currently only works with companies listed in the Indonesian Stock Market (IDX).
Accessible [here](https://idx-recommendation.herokuapp.com/)

![Image Not Available](https://gitlab.com/Emilfs/stock/-/raw/media/example.png?raw=true)

#### Run Locally
```console
$ pip install -r requirements.txt
$ python manage.py runserver
```

#### Technical Indicators
* MACD
* RSI
* Bollinger Band
* OBV

#### Based On
https://github.com/bukosabino/ta
