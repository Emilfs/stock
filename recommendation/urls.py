from django.urls import path

from . import views
from . import services

urlpatterns = [
    path('', views.index, name='index'),
    path('test/', services.get_daily, name='get_daily'),
]