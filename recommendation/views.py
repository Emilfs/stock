import requests
from django.shortcuts import render
from django.http import HttpResponseNotFound, JsonResponse
from datetime import date
from . import services

def index(request):
    symbol = "pgas.jk"
    if request.method == 'GET' and 'search-bar' in request.GET:
        symbol = str(request.GET['search-bar']) + ".jk"

    daily_data = services.get_daily(symbol)

    macd = services.macd(daily_data)
    rsi = services.rsi(daily_data)
    bb = services.bb(daily_data)
    obv = services.obv(daily_data)
    context = {'macd': macd, 'rsi': rsi, 'bb': bb, 'obv': obv, 'code': symbol.upper(), 'updated': date.today()}
    return render(request, 'recommendation/index.html', context)

