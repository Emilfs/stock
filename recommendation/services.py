import requests
import json
import pandas as pd
import ta
import time
from django.conf import settings

def get_daily(symbol):
    api_key = settings.SECRET_KEY
    end = int(time.time())
    start = end - 31622400
    url = "https://finnhub.io/api/v1/stock/candle?symbol=" + symbol + "&resolution=D&from=" + str(start) + "&to=" + str(end) + "&token=" + api_key

    response = requests.get(url)
    parsed = json.loads(response.text)

    open, high, low, close, volume = [], [], [], [], []
    for x in range(len(parsed["c"])):
        try:
            open.append(parsed["o"][x])
            high.append(parsed["h"][x])
            low.append(parsed["l"][x])
            close.append(parsed["c"][x])
            volume.append(parsed["v"][x])
        except KeyError:
            continue
    df = pd.DataFrame([open, high, low, close, volume]).T
    df = ta.utils.dropna(df)
    df.columns = ['Open', 'High', 'Low', 'Close', 'Volume']
    df.Open = pd.to_numeric(df.Open, errors='coerce')
    df.High = pd.to_numeric(df.High, errors='coerce')
    df.Low = pd.to_numeric(df.Low, errors='coerce')
    df.Close = pd.to_numeric(df.Close, errors='coerce')
    df.Volume = pd.to_numeric(df.Volume, errors='coerce')
    return df


def macd(df):
    indicator_macd = ta.trend.MACD(close=df["Close"], fillna=True)
    diff = indicator_macd.macd_diff()
    movement1 = (diff.iloc[-1] - diff.iloc[-2]) < 0
    movement2 = (diff.iloc[-2] - diff.iloc[-3]) < 0
    movement3 = (diff.iloc[-3] - diff.iloc[-4]) < 0
    if diff.iloc[-1] > 0:
        if movement1 and movement2 and movement3:
            return "SELL"
        else:
            return "HOLD"
    if(diff.iloc[-1] < 0):
        if not movement1 and not movement2 and not movement3:
            return "BUY"
        elif movement1 and movement2 and movement3:
            return "SELL"
        elif (diff.iloc[-1] - diff.iloc[-4]) < 0:
            return "SELL"
        else:
            return "HOLD"


def rsi(df):
    indicator_rsi = ta.momentum.RSIIndicator(close=df["Close"], fillna=True)
    last_rsi = indicator_rsi.rsi().get(indicator_rsi.rsi().size - 1)
    if last_rsi < 30:
        return "BUY"
    elif last_rsi > 70:
        return "SELL"
    else:
        return "HOLD"


def bb(df):
    indicator_bb = ta.volatility.BollingerBands(close=df["Close"], fillna=True)
    if indicator_bb.bollinger_lband_indicator().get(indicator_bb.bollinger_lband_indicator().size - 1) == 1:
        return "BUY"
    elif indicator_bb.bollinger_hband_indicator().get(indicator_bb.bollinger_hband_indicator().size - 1) == 1:
        return "SELL"
    else:
        return "HOLD"


def obv(df):
    # need help from MA(?)
    score = 0
    indicator_obv = ta.volume.OnBalanceVolumeIndicator(close=df["Close"], volume=df["Volume"])
    obv_diff = indicator_obv.on_balance_volume()
    if obv_diff.iloc[-1] - obv_diff.iloc[-2] > 0:
        score += 1
    if obv_diff.iloc[-2] - obv_diff.iloc[-3] > 0:
        score += 1
    if obv_diff.iloc[-3] - obv_diff.iloc[-4] > 0:
        score += 1
    if score >= 2:
        return "BUY"
    elif score == 1:
        return "HOLD"
    else:
        return "SELL"
